const fs = require('fs');
const path = require('path');

const dir = 'src/webp';
const addWebp = '.webp';

const readSubDirSync = (dir) => {
  let result = [];
  const readTopDirSync = (dir) => {
    let items = fs.readdirSync(dir);
    items = items.map((itemName) => {
      return path.join(dir, itemName);
    });
    items.forEach((itemPath) => {
      result.push(itemPath);
      if (fs.statSync(itemPath).isDirectory()) {
        readTopDirSync(itemPath);
      }
    });
  };
  readTopDirSync(dir);
  return result;
};

const targetFileNames = [];
const fileNameList = readSubDirSync(dir);
fileNameList.forEach((itemPath) => {
  if (itemPath.match(/.jpg/) || itemPath.match(/.png/)) {
    targetFileNames.push(itemPath);
  }
});

/* 拡張子で抽出したファイル名一覧「targetFileNames」を forEach で回す */
targetFileNames.forEach((fileName) => {
  const filePath = {};
  const newName = fileName + addWebp;

  filePath.before = path.join(fileName);
  filePath.after = path.join(newName);

  /* リネーム処理 */
  fs.rename(filePath.before, filePath.after, (err) => {
    if (err) throw err;
    console.log(filePath.before + '-->' + filePath.after);
  });
  /* END リネーム処理 */
});
