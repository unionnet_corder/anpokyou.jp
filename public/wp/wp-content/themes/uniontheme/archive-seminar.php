<?php
get_header(); ?>
<?php
$post_type = get_post_type();
?>
<div class="c-lower-head02">
  <div class="c-container">
    <?php if($post_type == 'member_seminar'):?>
      <h1 class="c-lower-head02__titl c-title04">
        <span class="en c-text-center">Seminar / Training</span>
        <span class="jp c-title01 c-text-center">会員限定セミナー・研修</span>
      </h1>
    <?php else: ?>
      <h1 class="c-lower-head02__titl c-title04">
        <span class="en c-text-center">Seminar / Training</span>
        <span class="jp c-title01 c-text-center">セミナー・研修</span>
      </h1>
    <?php endif; ?>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-seminar js-anim_elm -base">
  <div class="c-container">
  <div class="c-search">
    <dl class="c-search__item">
      <dt class="c-search__item__title">都道府県</dt>
      <dd class="c-search__item__content">
        <?php if($post_type == 'member_seminar'):?>
        <select class="c-select js-location_select" name="pref">
          <option value="/<?php echo $post_type; ?>" selected>全て</option>
          <?php $area_terms = get_terms('area_cat','hide_empty=0');
          foreach ($area_terms as $term):
            $name = esc_html($term->name);
            $slug = esc_html($term->slug);
          ?>
          <option value="/mypage/<?php echo $post_type; ?>/?area_cat=<?php echo $slug; ?>"><?php echo $name; ?></option>
          <?php endforeach;?>
        </select>
        <?php else: ?>
          <select class="c-select js-location_select" name="pref">
          <option value="/<?php echo $post_type; ?>" selected>全て</option>
            <?php $area_terms = get_terms('area_cat','hide_empty=0');
            foreach ($area_terms as $term):
              $name = esc_html($term->name);
              $slug = esc_html($term->slug);
            ?>
            <option value="/<?php echo $post_type; ?>/?area_cat=<?php echo $slug; ?>"><?php echo $name; ?></option>
            <?php endforeach;?>
          </select>
        <?php endif; ?>
      </dd>
    </dl>
  </div>
		<?php if (have_posts()) : ?>
		<ul class="c-seminar_list">
			<?php while (have_posts()) : the_post(); ?>
			<?php locate_template( 'parts/item-seminar.php', true, false ); ?>
			<?php endwhile; ?>
    </ul>
		<?php if (function_exists('wp_pagenavi')) : ?>
    <?php wp_pagenavi(); ?>
    <?php endif; ?>
		<?php else : ?>
		<p class="c-text01 u-mt20">まだ記事がありません。</p>
		<?php endif;?>
	</div>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php get_footer(); ?>
