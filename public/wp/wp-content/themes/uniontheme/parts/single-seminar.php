<?php
$eventday = CFS()->get('date');
$datetime = date_create($eventday);
$week = array ( '日', '月', '火', '水', '木', '金', '土' );
$w = (int)date_format($datetime, 'w');
$youbi = $week[$w];
$post_type = get_post_type();
?>
<section class="c-seminar_single__content">
  <div class="c-seminar_single__head">
    <h2 class="c-seminar_single__head__title"><?php the_title(); ?></h2>
    <time datetime="<?php echo date('Y年m月d日'.'('.$youbi.')',strtotime(CFS()->get('date'))); ?> <?php echo CFS()->get('time'); ?>" class="c-seminar_single__head__time"><?php echo date('Y年m月d日'.'('.$youbi.')',strtotime(CFS()->get('date'))); ?> <?php echo CFS()->get('time'); ?></time>
  </div>
  <div class="c-seminar_single__post">
    <figure>
      <?php if(has_post_thumbnail()): ?>
      <img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x, <?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
      <?php endif; ?>
    </figure>
    <?php the_content(); ?>
    <div class="c-seminar_single__post__info">
      <dl class="info-item">
        <dt class="info-item__title">受講区分</dt>
        <dd class="info-item__content"><?php echo CFS()->get('type'); ?></dd>
      </dl>
      <dl class="info-item">
        <dt class="info-item__title">開催日時</dt>
        <dd class="info-item__content">
          <time><?php echo date('Y年m月d日'.'('.$youbi.')',strtotime(CFS()->get('date'))); ?> <?php echo CFS()->get('time'); ?></time>
        </dd>
      </dl>
      <dl class="info-item">
        <dt class="info-item__title">講師</dt>
        <dd class="info-item__content">
          <p><?php echo CFS()->get('instructor'); ?></p>
          <?php
            $fields = CFS()->get('instructor-profile');
            if($fields):
            foreach ($fields as $field) :
          ?>
          <div class="instructor-profile">
            <p class="instructor-name"><?php echo $field['instructor-name']; ?></p>
            <div class="instructor-item">
              <?php
              $subfields = $field['instructor-item'];
              if($subfields):
              ?>
              <?php
              foreach ($subfields as $subfield):
              ?>
              <dl>
                <dt><?php echo $subfield['item-ttl']; ?>：</dt>
                <dd><?php echo $subfield['item-text']; ?></dd>
              </dl>
              <?php endforeach; endif; ?>
            </div>
          </div>
          <?php endforeach; ?>
          <?php endif; ?>
        </dd>
      </dl>
      <dl class="info-item">
        <dt class="info-item__title">参加費</dt>
        <dd class="info-item__content"><?php echo CFS()->get('price'); ?></dd>
      </dl>
      <dl class="info-item">
        <dt class="info-item__title">会場受講場所</dt>
        <dd class="info-item__content"><?php echo CFS()->get('place'); ?></dd>
      </dl>
      <dl class="info-item">
        <dt class="info-item__title">オンライン受講配信方法</dt>
        <dd class="info-item__content">
          <?php echo CFS()->get('onair'); ?>
          <!-- <p><strong>V-CUBE配信【アーカイブ配信付き】</strong></p>
          <ul>
            <li>オンライン受講ガイドをダウンロードいただき受講の流れを必ずご確認ください。</li>
            <li>開催1営業日前の13時にメールで視聴URLとPDF資料のご案内をお送りします。<br>(ハンズオンセミナーなど補足事項欄に送付日の記載がある場合はそちらに準じます)</li>
            <li>ご使用PC、ネットワークにかかるセキュリティ制限がある場合、ご視聴ができない場合がございますので事前に社内ご担当部署等にご確認をお願い致します。</li>
          </ul> -->
        </dd>
      </dl>
    </div>
    <div class="c-button01 -green -center">
      <a class="js-hover" href="<?php echo HOME; ?>mypage/seminar_contact/?post_id=<?php echo $post->ID; ?>">
      <?php if($post_type === 'member_seminar'):?>
        申し込む
      <?php else: ?>
        ログインして申し込む
      <?php endif; ?>
        <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/sign-in-alt-light.svg" alt=""></i>
      </a>
    </div>
  </div>
</section>