<?php
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<section class="p-login">
  <div class="c-container">
    <h1 class="c-title04">
      <span class="en c-text-center">Login</span>
      <span class="jp c-title01 c-text-center">総合員専用 ログイン画面</span>
    </h1>
    <div class="p-login__content">
      <div class="p-login__pic">
      </div>
      <form class="p-login_form" action="">
        <div class="p-login__inner">
          <?php the_content(); ?>
        </div>
      </form>
    </div>
  </div>
</section>

<?php get_footer(); ?>