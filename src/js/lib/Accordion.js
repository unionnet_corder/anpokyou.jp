export default class {
	constructor(elm) {
				document.addEventListener('DOMContentLoaded', () => {
			const accordionTrigger = document.querySelectorAll('.js-accordion-trigger');
			for (let i = 0; i < accordionTrigger.length; i++) {	
					if(accordionTrigger[i].classList.contains('is-opened')) {
							accordionTrigger[i].nextElementSibling.style.height = accordionTrigger[i].nextElementSibling.scrollHeight + 60 + 'px';
					}	
					accordionTrigger[i].addEventListener('click', (e) => {
							let currentElement = e.currentTarget;	
							let accordionTarget = currentElement.nextElementSibling;
							if (accordionTarget.style.height) {	
									currentElement.classList.remove('is-opened');
									accordionTarget.classList.remove('is-opened');			
									accordionTarget.style.height = null;	
							} else {
									currentElement.classList.add('is-opened');	
									accordionTarget.classList.add('is-opened');
									accordionTarget.style.height = accordionTarget.scrollHeight + 60 + 'px';
							}
					});
			}
    });
	}
}