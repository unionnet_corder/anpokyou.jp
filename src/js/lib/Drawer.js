export default function () {
  document.addEventListener('DOMContentLoaded', () => {
    if (document.getElementById('js-drawer') != null && document.getElementsByClassName('js-drawer-button').length > 0) {
      let isAct = false
      const documentHtml = document.documentElement
      const documentBody = document.body
      const drawer = document.getElementById('js-drawer')
      const header = document.getElementById('js-header')
      const drawerLink = drawer.querySelectorAll('a')
      const node = Array.prototype.slice.call(drawerLink, 0)
      const drawerButton = document.getElementsByClassName('js-drawer-button')
      const drawerButtonLength = drawerButton.length
      const open = () => {
        documentHtml.style.overflow = 'hidden'
        documentBody.style.overflow = 'hidden'
        drawer.classList.add('is-act')
        header.classList.add('is-act')
        for (var i = 0; i < drawerButtonLength; i++) {
          drawerButton[i].classList.add('is-act')
        }
      }
      const close = () => {
        documentHtml.style.overflow = 'auto'
        documentBody.style.overflow = 'auto'
        drawer.classList.remove('is-act')
        header.classList.remove('is-act')
        for (var i = 0; i < drawerButtonLength; i++) {
          drawerButton[i].classList.remove('is-act')
        }
      }
      drawer.children[0].addEventListener('click', function (e) {
        e.stopPropagation()
      })
      drawer.addEventListener('click', close)

      node.forEach((e) => {
        e.addEventListener('click', function (e) {
          if (this.getAttribute('href').indexOf('#') == 0) {
            e.preventDefault()
            close()
          }
        })
      })
      for (var i = 0; i < drawerButtonLength; i++) {
        drawerButton[i].addEventListener('click', () => {
          if (isAct) {
            isAct = false
            close()
          } else {
            isAct = true
            open()
          }
        })
      }
    }
  })
}
