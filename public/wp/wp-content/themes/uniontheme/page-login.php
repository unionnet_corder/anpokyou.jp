<?php
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<section class="p-login">
  <div class="c-container">
    <h1 class="c-title04">
      <span class="en c-text-center">Login</span>
      <span class="jp c-title01 c-text-center">総合員専用 ログイン画面</span>
    </h1>
    <div class="p-login__content">
      <div class="p-login__pic">
      </div>
      <form class="p-login_form my_form" name="my_login_form" id="my_login_form" action="" method="post">
        <div class="p-login__inner">
          <div class="p-login__inner__input">
            <input class="c-text01" id="login_user_name" name="user_name" type="text" placeholder="ユーザー名 または メールアドレス">
            <input class="c-text01" id="login_password" name="user_pass" id="user_pass" type="password" placeholder="パスワード">
            <span class="mwform-checkbox-field horizontal-item">
              <label class="input-checkbox">
                <input name="rememberme" type="checkbox" id="rememberme" value="forever">
                <label class="mwform-checkbox-field-text c-text01" for="rememberme">ログイン状態を保存する</label>
              </label>
            </span>
          </div>
          <div class="p-login__inner__link">
            <div class="button">
              <button type="submit" name="my_submit" class="c-button01 -green -small" value="login">
              <span class="submit js-hover">
                  ログイン
                  <i class="ico"><img class="js-svg" src="../img/common/ico/sign-in-alt-light.svg" alt=""></i>
                </span>
              </button>
            </div>
            <!-- <ul>
              <li><a class="js-hover link" href="/wp-login.php?action=lostpassword">パスワードを忘れた方はこちら</a></li>
              <li><a class="js-hover link" href="">会員登録についてはこちら</a></li>
            </ul> -->
          </div>
        </div>
        <?php wp_nonce_field( 'my_nonce_action', 'my_nonce_name' );  //nonceフィールド設置 ?>
      </form>
    </div>
  </div>
</section>

<?php get_footer(); ?>

