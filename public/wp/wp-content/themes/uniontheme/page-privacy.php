<?php
/*
Template Name: プライバシーポリシー
Union Theme - Version: 1.4
*/

get_header(); ?>

<section class="p-privacy">
<div class="c-container">

<h2 class="c-title06 js-anim_elm -base u-mb40">プライバシーポリシー</h2>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">はじめに</h3>
<p class="c-text01">
一般社団法人 安全保育推進連絡協議会（以下、「当法人」）は、各種サービスのご提供にあたり、お客様の個人情報をお預かりしております。<br>
当法人は個人情報を保護し、お客様に更なる信頼性と安心感をご提供できるように努めて参ります。<br>
当法人は、個人情報に関する法令を遵守し、個人情報の適切な取り扱いを実現いたします。</p>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">
1. 個人情報の取得について</h3>
<p class="c-text01">
当法人は、偽りその他不正の手段によらず適正に個人情報を取得いたします。なお、次のような場合に必要な範囲で個人情報を収集する場合があります。</p>
<ol class="nl">
<li>ご相談・お問い合わせ</li>
</ol>

</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">
2. 個人情報の利用について</h3>
<p class="c-text01">
当法人は、個人情報を以下の利用目的の達成に必要な範囲内で、利用いたします。<br>
以下に定めのない目的で個人情報を利用する場合、あらかじめご本人の同意を得た上で行ないます。</p>
<ol class="nl">
<li>ご相談・お問い合わせに対する回答や確認のご連絡のため</li>
<li>個人情報を特定しない統計情報に利用するため</li>
</ol>

</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">3. 個人情報の安全管理について</h3>
<p class="c-text01">
当法人は、取り扱う個人情報の漏洩、滅失またはき損の防止その他の個人情報の安全管理のために必要かつ適切な措置を講じます。
</p>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">4. 個人情報の委託について</h3>
<p class="c-text01">
当法人は、個人情報の取り扱いの全部または一部を第三者に委託する場合は、当該第三者について厳正な調査を行い、取り扱いを委託された個人情報の安全管理が図られるよう当該第三者に対する必要かつ適切な監督を行います。</p>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">5. 個人情報の第三者提供について</h3>
<p class="c-text01">
当法人は、個人情報保護法等の法令に定めのある場合を除き、個人情報をあらかじめご本人の同意を得ることなく、第三者に提供いたしません。</p>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">6. 個人情報の開示・訂正等について</h3>
<p class="c-text01 u-mb10">
当法人は、ご本人から自己の個人情報についての開示の請求がある場合、速やかに開示をいたします。<br>
その際、ご本人であることが確認できない場合には、開示に応じません。<br>
個人情報の内容に誤りがあり、ご本人から訂正・追加・削除の請求がある場合、調査の上、速やかにこれらの請求に対応いたします。<br>
その際、ご本人であることが確認できない場合には、これらの請求に応じません。<br>
当法人の個人情報の取り扱いにつきまして、上記の請求・お問い合わせ等ございましたら、下記までご連絡くださいますようお願い申し上げます。
</p>
		<table>
			<caption class="txtL u-mb10">
			【　連絡先　】
			</caption>
			<col style="width:25%">
			<col style="width:auto">
			<tr>
				<th scope="row">名称</th>
				<td>一般社団法人　安全保育推進連絡協議会</td>
			</tr>
			<tr>
				<th scope="row">所在地</th>
				<td>〒569-0803<br>大阪府高槻市高槻町5-23　ファイブビル5階</td>
			</tr>
			<tr>
				<th scope="row">電話番号</th>
				<td>072-681-7465（株式会社パワフルケア）</td>
			</tr>
			<tr>
				<th scope="row">メールアドレス</th>
				<td>info@anpokyou.jp</td>
			</tr>
		</table>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">7. 組織・体制</h3>
<p class="c-text01">
当法人は、代表者を個人情報管理責任者とし、個人情報の適正な管理及び継続的な改善を実施いたします。</p>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">8. その他の注意事項</h3>
<p class="c-text01">
当法人が運営するコンテンツや掲載広告などからリンクされている第三者のサイト及びサービスは、当法人とは独立した個人情報の保護に関する規定やデータ収集の規約を定めています。<br>
当サイトはこれらの規約や活動に対していかなる義務や責任も負いません。</p>
</section>

<section class="js-anim_elm -base u-mb30">
<h3 class="c-title02 u-mb10">9. 個人情報の管理方法の継続的改善について</h3>
<p class="c-text01">
当法人は、個人情報の管理方法を見直し、継続的に改善を実施します。</p>
</section>

<section class="js-anim_elm -base">
<h3 class="c-title02 u-mb10">10. 本方針の変更</h3>
<p class="c-text01">
本方針の内容は変更されることがあります。<br>
変更後の本方針については、当法人が別途定める場合を除いて、当サイトに掲載した時から効力を生じるものとします。</p>

</section>
<!-- /.c-container --></div>
</section>

<?php get_footer(); ?>