<?php
get_header();  ?>

<div class="c-lower-head02">
  <div class="c-container">
    <?php if($post_type == 'news_qa'):?>
      <h1 class="c-lower-head02__titl c-title04">
        <span class="en c-text-center">News / Q&A</span>
        <span class="jp c-title01 c-text-center">お知らせ・Q&A</span>
      </h1>
    <?php else: ?>
      <h1 class="c-lower-head02__titl c-title04">
        <span class="en c-text-center">News</span>
        <span class="jp c-title01 c-text-center">お知らせ</span>
      </h1>
    <?php endif; ?>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<?php while (have_posts()) : the_post(); ?>
<article class="p-news_single js-anim_elm -base">
  <div class="c-container -small">
    <div class="c-post">
      <div class="c-post_head">
        <h1><?php the_title(); ?></h1>
        <div class="c-post_head__info">
          <time datetime="<?php the_time('Y-m-d'); ?>" class="c-text01"><?php the_time('Y.m.d'); ?></time>
          <?php $terms = get_the_terms(get_the_ID(), 'category'); if (!empty($terms)) : ?>
          <?php foreach ($terms as $term) : ?>
            <span class="cat"><?php echo $term->name; ?></span>
          <?php endforeach; ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="c-post_body">
        <?php the_content(); ?>
      </div>
      <div class="c-button01 -green -small">
        <?php if($post_type == 'news_qa'):?>
        <a class="js-hover" href="/mypage/news_qa/">
          一覧へ
          <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
        </a>
        <?php else: ?>
          <a class="js-hover" href="/info/">
            一覧へ
            <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
          </a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</article>
<?php endwhile;?>
<?php get_footer(); ?>