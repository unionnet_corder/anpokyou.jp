<?php
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<div class="p-index_kv">
  <div class="p-index_kv__inner js-anim_elm">
    <img class="u-visible_pc" src="./img/main/index_kv.jpg" srcset="./img/main/index_kv.jpg 1x,./img/main/index_kv@2x.jpg 2x" alt="">
    <img class="u-visible_tab" src="./img/main/index_kv_sp.jpg" srcset="./img/main/index_kv_sp.jpg 1x,./img/main/index_kv_sp@2x.jpg 2x" alt="">
  <!-- /.p-index_kv__inner --></div>
  <div class="p-index_kv__copy">
    <div class="p-index_kv__copy01">
      <span>手を取り合い</span>
      <span>未来のための</span>
      <span>保育を支援</span>
    </div>
    <p class="p-index_kv__copy02">We seek the best for children and parents.</p>
  </div>
<!-- /.p-index_kv --></div>
<section class="p-index_about">
  <div class="p-index_about__container">
    <div class="c-container">
      <div class="p-index_about__inner">
        <div class="c-title01 p-index_about__title">
          <p class="c-title01__sub js-sprit js-anim_elm -blur-text">About us</p>
          <h2 class="c-title01__lead js-anim_elm -blur -blur_bottom">当会について</h2>
        </div>
        <h3 class="c-title02 p-index_about__lead js-sprit js-anim_elm -blur -blur_bottom">企業主導型保育事業者を中心とした企業・団体の支援を通じて、働く女性、子育て世帯の支援をします。</h3>
        <p class="c-text02 p-index_about__text js-anim_elm -base">私たちは待機児童解消と働く女性の活躍支援・子育て支援を推し進めることを目的としています。<br>当会に所属する事業者が独自性と多様性をもって、健全な経営を行い、結果として子どもと親の最善の利益を守るため、事業者同士の意識やノウハウの共有、行政への提言や要望、行政と事業者との橋渡し、業界団体同士の連携・業界の活性化など、多岐にわたる役割を担います。</p>
        <div class="c-button01 js-anim_elm -base">
          <a class="js-hover" href="/about-us">
            組合情報を詳しく知る
            <i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>
          </a>
        </div>
      </div>
      <div class="p-index_about__pic">
        <span class="js-anim_elm -fade">
          <img src="./img/index/about_01.jpg" srcset="./img/index/about_01.jpg 1x,./img/index/about_01@2x.jpg 2x" alt="">
        </span>
      </div>
    </div>
  </div>
<!-- /.p-index_about --></section>
<section class="p-index_service">
  <div class="c-container">
    <div class="c-title03 js-anim_elm -base">
      <p class="c-text-center c-title03__sub js-sprit js-anim_elm -blur-text">会員になることで受けられる</p>
      <h2 class="c-text-center c-title03__lead js-anim_elm -blur -blur_bottom">サービス</h2>
    </div>
    <div class="p-index_service__inner">
      <ul class="p-index_service__list">
        <li class="item js-anim_elm -order-fade">
          <div class="item__head">
            <h3 class="item__head__text">Service <span>01</span></h3>
            <span class="item__head__img">
              <img class="js-svg" src="./img/index/service_img01.svg" alt="">
            </span>
          </div>
          <h4 class="item__lead c-text03 -green c-font-zen">保育所よろず相談</h4>
          <p class="c-text01">保育・マネジメント・制度解釈に対する質問等を会員様のフォームより受け付けます。 回答は個別に行いますが、差し支えなければサイト内にて会員で共有いたします。</p>
        </li>
        <li class="item js-anim_elm -order-fade">
          <div class="item__head">
            <h3 class="item__head__text">Service <span>02</span></h3>
            <span class="item__head__img">
              <img class="js-svg" src="./img/index/service_img02.svg" alt="">
            </span>
          </div>
          <h4 class="item__lead c-text03 -green c-font-zen">賛助企業によるサービス提供・商品販売</h4>
          <p class="c-text01">備品・消耗品などのハードからホームページ制作・福利厚生など、賛助としてご紹介。 購入の際には会員待遇も。</p>
        </li>
        <li class="item js-anim_elm -order-fade">
          <div class="item__head">
            <h3 class="item__head__text">Service <span>03</span></h3>
            <span class="item__head__img">
              <img class="js-svg" src="./img/index/service_img03.svg" alt="">
            </span>
          </div>
          <h4 class="item__lead c-text03 -green c-font-zen">保育士向け、マネージャー・<br>経営者向け研修</h4>
          <p class="c-text01">キャリアアップをはじめとする保育に関するさまざまな魅力的な研修を受講できます。</p>
        </li>
      </ul>
      <div class="p-index_service__button">
        <div class="c-button01 -green js-anim_elm -base">
          <a class="js-hover" href="<?php echo HOME; ?>join-us/">
            入会のご案内
            <i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>
          </a>
        </div>
        <div class="c-button01 js-anim_elm -base">
          <a class="js-hover" href="<?php echo HOME; ?>login/">
            会員ログイン
            <i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>
          </a>
        </div>
      </div>
    </div>
  </div>
<!-- /.p-index_service --></section>

<?php $args = array(
'post_type' => 'member',
'posts_per_page' => 5,
); $the_query = new WP_Query($args); if ($the_query->have_posts()) : ?>
<section class="p-index_member">
  <div class="c-container">
    <div id="member-slider" class="swiper p-index_member__slider">
      <div class="p-index_member__slider__head">
        <div class="c-title01 js-anim_elm -base">
          <p class="c-title01__sub js-sprit js-anim_elm -blur-text">Member</p>
          <h2 class="c-title01__lead js-anim_elm -blur">会員情報</h2>
        </div>
        <!-- 前ページボタン -->
        <div class="p-index_member__slider__button js-anim_elm -base">
          <div class="swiper-button-prev js-hover">
            <i class="ico"><img class="js-svg" src="./img/common/ico/angle-left-light.svg" alt=""></i>
          </div>
          <!-- 次ページボタン -->
          <div class="swiper-button-next js-hover">
            <i class="ico"><img class="js-svg" src="./img/common/ico/angle-right-light.svg" alt=""></i>
          </div>
        </div>
      </div>

      <ul class="swiper-wrapper p-index_member__slider__list">
        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
        <li class="swiper-slide item js-anim_elm -order-fade">
          <a class="js-hover" href="<?php the_permalink(); ?>">
            <div class="item__img">
            <?php if(has_post_thumbnail()): ?>
						<img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x,<?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
						<?php endif; ?>
            </div>
            <p class="item__text c-text03"><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i><?php the_title(); ?></p>
            <ul class="item__tag">
              <li>#<?php if ($terms = get_the_terms($post->ID, 'area_cat')) {
                    foreach ($terms as $term) {
                      echo $term->name;
                    }
                } ?></li>
            </ul>
          </a>
        </li>
        <?php endwhile; ?>
      </ul>
    </div>
    <div class="p-index_support-company__button c-button01 js-anim_elm -base">
      <a class="js-hover" href="/member">
        会員一覧へ
        <i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>
      </a>
    </div>
  </div>
<!-- /.p-index_member --></section>
<?php endif; wp_reset_postdata(); ?>

<section class="p-index_support-company">
  <div class="c-container">
    <div class="p-index_support-company__bnr js-anim_elm -base">
      <a class="hover js-anim_elm -scale" href="/support-company">
        <div class="image">
          <img src="./img/index/supporting_bnr.jpg" srcset="./img/index/supporting_bnr.jpg 1x,./img/index/supporting_bnr@2x.jpg 2x" alt="">
        </div>
        <div class="button">
          <div class="title">
            <p class="title_sub">Supporting company</p>
            <h2 class="title_main">賛助企業一覧はこちら</h2>
          </div>
          <i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>
        </div>
      </a>
    </div>
    <!--<div id="company-slider" class="swiper p-index_support-company__slider">
      <div class="p-index_support-company__slider__head">
        <div class="c-title01 js-anim_elm -base">
          <p class="c-title01__sub">Supporting company</p>
          <h2 class="c-title01__lead">賛助企業</h2>
        </div> -->
        <!-- 前ページボタン -->
        <!-- <div class="p-index_support-company__slider__button js-anim_elm -base">
          <div class="swiper-button-prev js-hover">
            <i class="ico"><img class="js-svg" src="./img/common/ico/angle-left-light.svg" alt=""></i>
          </div> -->
          <!-- 次ページボタン -->
           <!-- <div class="swiper-button-next js-hover">
            <i class="ico"><img class="js-svg" src="./img/common/ico/angle-right-light.svg" alt=""></i>
          </div>
        </div>
      </div> -->
      <!-- <ul class="swiper-wrapper p-index_support-company__slider__list">
        <li class="swiper-slide item js-anim_elm -order-fade">
          <a class="js-hover" href="">
            <div class="item__img">
              <img src="./img/index/company_logo.png" srcset="./img/index/company_logo.png 1x,./img/index/company_logo.png 2x" alt="">
            </div>
            <p class="item__text c-text03"><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>会社名が入ります</p>
            <ul class="item__tag">
              <li>#大阪府</li>
              <li>#業種名が入ります</li>
            </ul>
          </a>
        </li>
        <li class="swiper-slide item js-anim_elm -order-fade">
          <a class="js-hover" href="">
            <div class="item__img">
              <img src="./img/index/company_logo.png" srcset="./img/index/company_logo.png 1x,./img/index/company_logo.png 2x" alt="">
            </div>
            <p class="item__text c-text03"><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>会社名が入ります</p>
            <ul class="item__tag">
              <li>#大阪府</li>
              <li>#業種名が入ります</li>
            </ul>
          </a>
        </li>
        <li class="swiper-slide item js-anim_elm -order-fade">
          <a class="js-hover" href="">
            <div class="item__img">
              <img src="./img/index/company_logo.png" srcset="./img/index/company_logo.png 1x,./img/index/company_logo.png 2x" alt="">
            </div>
            <p class="item__text c-text03"><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>会社名が入ります</p>
            <ul class="item__tag">
              <li>#大阪府</li>
              <li>#業種名が入ります</li>
            </ul>
          </a>
        </li>
        <li class="swiper-slide item js-anim_elm -order-fade">
          <a class="js-hover" href="">
            <div class="item__img">
              <img src="./img/index/company_logo.png" srcset="./img/index/company_logo.png 1x,./img/index/company_logo.png 2x" alt="">
            </div>
            <p class="item__text c-text03"><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>会社名が入ります</p>
            <ul class="item__tag">
              <li>#大阪府</li>
              <li>#業種名が入ります</li>
            </ul>
          </a>
        </li>
        <li class="swiper-slide item js-anim_elm -order-fade">
          <a class="js-hover" href="">
            <div class="item__img">
              <img src="./img/index/company_logo.png" srcset="./img/index/company_logo.png 1x,./img/index/company_logo.png 2x" alt="">
            </div>
            <p class="item__text c-text03"><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>会社名が入ります</p>
            <ul class="item__tag">
              <li>#大阪府</li>
              <li>#業種名が入ります</li>
            </ul>
          </a>
        </li>
      </ul> -->
   <!--  </div>
    <div class="p-index_support-company__button c-button01 js-anim_elm -base">
      <a class="js-hover" href="">
        賛助企業一覧へ
        <i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i>
      </a>
    </div> -->
  </div>
<!-- /.p-index_support-company --></section>


<section class="p-index_info">
  <div class="p-index_info__container">
    <?php $args = array(
    'post_type' => 'post',
    'posts_per_page' => 8,
    ); $the_query = new WP_Query($args); if ($the_query->have_posts()) : ?>
    <section class="p-index_news">
      <div class="p-index_info__head">
        <div class="c-title01 js-anim_elm -base">
          <p class="c-title01__sub js-sprit js-anim_elm -blur-text">News</p>
          <h2 class="c-title01__lead js-anim_elm -blur">お知らせ</h2>
        </div>
        <div class="c-button02 js-anim_elm -base u-visible_pc">
          <a class="js-hover" href="/info"><span class="js-sprit">一覧へ</span><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i></a>
        </div>
      </div>
      <div class="p-index_news__content js-anim_elm -base">
        <ul class="c-news_list">
          <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
          <?php locate_template( 'parts/item-news.php', true, false ); ?>
          <?php endwhile; ?>
        </ul>
      </div>
      <div class="c-button01 js-anim_elm -base u-visible_sp">
        <a href="/info"><span class="js-sprit">一覧へ</span><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i></a>
      </div>
    <!-- /.p-index_news --></section>
    <?php endif; wp_reset_postdata(); ?>

    <?php $args = array(
    'post_type' => 'seminar',
    'posts_per_page' => 4,
    ); $the_query = new WP_Query($args); if ($the_query->have_posts()) : ?>
    <section class="p-index_seminar">
      <div class="p-index_info__head">
        <div class="c-title01 js-anim_elm -base">
          <p class="c-title01__sub js-sprit js-anim_elm -blur-text">Seminar / Training</p>
          <h2 class="c-title01__lead js-anim_elm -blur">セミナー・研修</h2>
        </div>
        <div class="c-button02 js-anim_elm -base u-visible_pc">
          <a class="js-hover" href="/seminar">
            <span class="js-sprit">一覧へ</span><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i></a>
        </div>
      </div>
      <div class="p-index_seminar__content js-anim_elm -base">
        <ul class="p-index_seminar__list">
          <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
          <?php locate_template( 'parts/item-seminar.php', true, false ); ?>
          <?php endwhile; ?>
        </ul>
      </div>
      <div class="c-button01 js-anim_elm -base u-visible_sp">
        <a href="/seminar"><span class="js-sprit">一覧へ</span><i class="ico"><img class="js-svg" src="./img/common/ico/chevron-circle-right-light.svg" alt=""></i></a>
      </div>
    <!-- /.p-index_seminar --></section>
    <?php endif; wp_reset_postdata(); ?>
  </div>
<!-- /.p-index_info --></section>

<?php locate_template( 'parts/footer-head.php', true, false ); ?>
<?php get_footer(); ?>