<div class="l-footer_head js-anim_elm -base">
  <div class="l-footer_head__item js-hover">
    <div class="l-footer_head__lead">
      <span>Admission</span>
    </div>
    <h2 class="l-footer_head__title c-text-center">入会を希望する</h2>
    <div class="c-button01 -green">
      <a class="js-hover" href="<?php echo HOME; ?>join-us/">
        詳しくはこちら
        <i class="ico"><img class="js-svg" src="/img/common/ico/chevron-circle-right-light.svg" alt=""></i>
      </a>
    </div>
  </div>
  <div class="l-footer_head__item js-hover">
    <div class="l-footer_head__lead">
      <span>Contact</span>
    </div>
    <h2 class="l-footer_head__title c-text-center">お問い合わせ</h2>
    <div class="c-button01 -green -ico-left">
      <a class="js-hover" href="<?php echo HOME; ?>contact/">
        <i class="ico"><img class="js-svg" src="/img/common/ico/envelope-light.svg" alt=""></i>
        メールで問い合わせる
      </a>
    </div>
  </div>
</div>