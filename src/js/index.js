'use strict'

require('picturefill')
require('intersection-observer')

import HoverClass from './lib/HoverClass'
import HoverOpacity from './lib/HoverOpacity'
// import Budoux from './lib/Budoux'
// import ShowPagetop from './lib/ShowPagetop'
import Smooth from './lib/Smooth'
import TelGrant from './lib/TelGrant'
import EqualHeight from './lib/EqualHeight'
import Drawer from './lib/Drawer'
import Svg from './lib/Svg'
import ScrollAnimation from './lib/ScrollAnimation'
import IeModal from './lib/IeModal'
import Accordion from './lib/Accordion'
import TextSprit from './lib/TextSprit'
// import CompanySlide from './lib/CompanySlide'
import MemberSlide from './lib/MemberSlide'
import SelectChenge from './lib/SelectChenge'
import Zipcode from './lib/Zipcode';

new HoverClass()
new HoverOpacity()
// // new Budoux()
// new ShowPagetop()
new Smooth()
new TelGrant()
new EqualHeight('.js-max_height')
new Drawer()
new Svg()
new ScrollAnimation()
new IeModal('サイト名', 'サイトURL')
new Accordion()
new TextSprit()
// new CompanySlide()
new MemberSlide()
new SelectChenge()
new Zipcode();
