<?php
get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php
$post_type = get_post_type();
?>
<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">Seminar / Training</span>
      <span class="jp c-title01 c-text-center">セミナー・研修</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-seminar_single c-seminar_single js-anim_elm -base">
  <div class="c-container c-seminar_single__container">

  <?php locate_template( 'parts/single-seminar.php', true, false ); ?>

    <section class="c-seminar_single__side">
      <h2 class="c-seminar_single__side__title">おすすめのセミナー・研修</h2>
      <?php
        $args = array(
          'connected_type' => 'posts_to_seminar',
          'connected_items' => get_queried_object(),
          'nopaging' => true,
          'suppress_filters' => false
        );
        $connected_posts = get_posts( $args ); ?>
      <ul class="c-seminar_single__side__list">
        <?php
        foreach ( $connected_posts as $post ) {
        setup_postdata( $post ); ?>
        <li class="c-seminar_list__item">
          <a href="<?php the_permalink(); ?>" class="c-seminar_list__link js-hover">
            <div class="c-seminar_list__link__img">
            <?php if(has_post_thumbnail()): ?>
						<img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x,<?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
						<?php endif; ?>
            </div>
            <div class="c-seminar_list__link__text">
              <dl class="c-seminar_list__link__desc">
                <dt>開催日</dt>
                <dd><time datetime="<?php echo date('Y年m月d日',strtotime(CFS()->get('date'))); ?>"><?php echo date('Y年m月d日',strtotime(CFS()->get('date'))); ?></time></dd>
              </dl>
              <dl class="c-seminar_list__link__desc">
                <dt>開催場所</dt>
                <dd><?php echo CFS()->get('place'); ?></dd>
              </dl>
              <p class="c-seminar_list__link__title c-text02"><?php the_title(); ?></p>
            </div>
          </a>
        </li>
        <?php }wp_reset_postdata();?>
      </ul>
    </section>
  </div>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php endwhile; endif; get_footer(); ?>