<dl class="p-faq__list__item">
  <dt class="p-faq__button js-accordion-trigger">
    <?php esc_html(block_field('question')); ?>
  </dt>
  <dd class="p-faq__modal"><span class="answer"></span><?php esc_html(block_field('answer')); ?></dd>
</dl>