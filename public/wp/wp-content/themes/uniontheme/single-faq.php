<?php 
get_header(); ?>
<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">Q&amp;A</span>
      <span class="jp c-title01 c-text-center">よくあるご質問</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-faq js-anim_elm -base">
  <div class="c-container">
  <?php if (have_posts()) : ?>
    <div class="p-faq__list js-anim_elm -base">
      <?php while (have_posts()) : the_post(); ?>
      <dl class="p-faq__list__item">
        <dt class="p-faq__button js-accordion-trigger">
          <?php the_title(); ?>
        </dt>
        <dd class="p-faq__modal"><span class="answer"></span><?php the_content(); ?></dd>
      </dl>
      <?php endwhile; ?>
    </div>
    <?php if (function_exists('wp_pagenavi')) : ?>
      <?php wp_pagenavi(); ?>
    <?php endif; ?>
    <?php else : ?>
    </div>
    <p class="c-text01">まだ質問がありません。</p>
		<?php endif; wp_reset_postdata(); ?>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php get_footer(); ?>
