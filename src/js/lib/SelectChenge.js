export default class {
  constructor() {
    const selected = document.querySelector(".js-location_select");
    if(!selected) return
    selected.onchange = function() {
        window.location.href = selected.value;
    };
  }
}