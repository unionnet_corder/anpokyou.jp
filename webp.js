const keepfolder = require('imagemin-keep-folder');
const webp = require('imagemin-webp');

keepfolder(['src/webp/**/*.webp'], {
  plugins: [
    webp({
      quality: 75,
    }),
  ],
  replaceOutputDir: (output) => {
    return output.replace(/src\/webp\//, 'public/img/');
  },
});
