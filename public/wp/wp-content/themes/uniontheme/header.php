<?php
/*
 Union Theme - Version: 2.0
*/
?>
<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" lang="ja">
<head>
<meta charset="UTF-8">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="https://www.anpokyou.jp/wp/wp-content/uploads/2022/03/favicon.ico">
<link rel="icon" type="image/vnd.microsoft.icon" href="https://www.anpokyou.jp/wp/wp-content/uploads/2022/03/favicon.ico">
<link rel="shortcut icon" type="image/x-icon" href="https://www.anpokyou.jp/wp/wp-content/uploads/2022/03/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="<?php echo HOME; ?>img/common/meta/webclip.png">
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@600&family=Zen+Maru+Gothic:wght@400;500;700&display=swap" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/js-cookie/2.2.1/js.cookie.min.js"></script></head>
<?php wp_head(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2KJJNT0XJB"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2KJJNT0XJB');
</script>

</head>

<body <?php body_class(); ?>>
<script>
	var cookieName = 'uni-anpokyou';
	var cookie = Cookies.get(cookieName);
	var body = document.body;
	if (!cookie) {
		var isOpening = true;
		var isLoaded = false;
		body.className += ' is-opening is-start';
		document.addEventListener('DOMContentLoaded', function () {
			body.className += ' is-ready';
			window.addEventListener('load', function () {
				body.className += ' is-loaded';
				isLoaded = true;
			});
		});
		var date = new Date();
		date.setTime(date.getTime() + (10 * 60 * 1000));
		Cookies.set(cookieName, '1', {
			expires: date,
			samesite: 'lax'
		});
	} else {
		body.className += ' is-start';
		document.addEventListener('DOMContentLoaded', function () {
			body.className += ' is-ready';
			window.addEventListener('load', function () {
				body.className += ' is-loaded';
			});
		});
	}
</script>

<div id="page">

<!--   ヘッダー   -->
<header id="js-header" class="l-header">
    <div class="l-header__container">
		<?php if(is_front_page() && !is_paged()) : ?>
      <h1 class="l-header__logo">
        <span class="link">
          <img src="<?php echo HOME; ?>img/common/logo.svg" alt="一般社団法人　安全保育推進連絡協議会">
        </span>
      </h1>
			<?php else: ?>
				<div class="l-header__logo">
					<a href="<?php echo HOME; ?>">
						<span class="link">
          		<img src="<?php echo HOME; ?>img/common/logo.svg" alt="一般社団法人　安全保育推進連絡協議会">
        		</span>
					</a>
				</div>
			<?php endif; ?>
			<?php if (is_page('mypage') || is_parent_slug() === 'mypage' || is_post_type_archive(array('member_seminar', 'news_qa')) || is_singular(array('member_seminar', 'news_qa'))) :?>
				<ul class="l-header__navi">
					<li>
						<a href="<?php echo HOME; ?>" class="js-sprit">HOME</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>mypage/news_qa/" class="js-sprit">お知らせ・Q&amp;A</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>mypage/member_seminar/" class="js-sprit">セミナー・研修</a>
					</li>
					<li class="c-button01 -blue -ico-left -small">
						<a class="js-hover" href="<?php echo HOME; ?>mypage/consultation">
							<i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/clipboard-list-regular.svg" alt=""></i>
							なんでもよろず相談
						</a>
					</li>
				</ul>
			<?php else: ?>
				<ul class="l-header__navi">
					<li>
						<a href="<?php echo HOME; ?>about-us/" class="js-sprit">組合情報</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>support-company/" class="js-sprit">賛助企業</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>info/" class="js-sprit">お知らせ</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>seminar/" class="js-sprit">セミナー・研修</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>member/" class="js-sprit">会員一覧</a>
					</li>
					<li>
						<a href="<?php echo HOME; ?>faq/" class="js-sprit">よくあるご質問</a>
					</li>
				</ul>
			<?php endif; ?>
    </div>
  </header>
  <main class="l-main">


