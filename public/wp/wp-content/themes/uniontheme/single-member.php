<?php
 get_header(); if (have_posts()) : while (have_posts()) : the_post(); $cfs = CFS(); ?>
 <?php $terms = get_the_terms(get_the_ID(), 'area_cat'); ?>
<article class="p-member_single">
  <div class="p-member_single__container js-anim_elm -base">
    <div class="c-container">
      <div class="p-member_single__head">
        <div class="p-member_single__head__inner">
          <h1 class="p-member_single__head__title"><?php the_title(); ?></h1>
          <dl class="p-member_single__head__info">
            <dt class="title c-text02">所在地</dt>
            <dd class="text c-text02"><?php echo $cfs->get('address'); ?></dd>
            <dt class="title c-text02">TEL</dt>
            <dd class="text c-text02"><?php echo $cfs->get('tel'); ?></dd>
            <dt class="title c-text02">FAX</dt>
            <dd class="text c-text02"><?php echo $cfs->get('fax'); ?></dd>
            <dt class="title c-text02">定員</dt>
            <dd class="text c-text02"><?php echo $cfs->get('capacity'); ?></dd>
            <dt class="title c-text02">開園時間</dt>
            <dd class="text c-text02"><?php echo $cfs->get('time'); ?></dd>
            <dt class="title c-text02">アクセス</dt>
            <dd class="text c-text02"><?php echo $cfs->get('access'); ?></dd>
          </dl>
          <div class="p-member_single__head__map c-button03">
            <a target="_blank" class="js-hover" href="<?php echo $cfs->get('google_map_url'); ?>">
              <i class="ico"><img class="js-svg" src="../../img/common/ico/map-pin.svg" alt=""></i>Google Maps
            </a>
          </div>
        </div>
        <div class="p-member_single__head__pic js-anim_elm -base">
          <span class="js-anim_elm -fade">
            <?php if(has_post_thumbnail()): ?>
              <img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x, <?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
            <?php endif; ?>
          </span>
        </div>
      </div>
      <table class="c-table">
        <tr>
          <th>日祝利用</th>
          <th>夜間利用</th>
          <th>一時利用</th>
          <th>病児・病後児保育</th>
          <th>地域児童枠</th>
          <th>共同利用枠</th>
        </tr>
        <tr>
          <td>
          <?php if ( get_post_meta($post->ID, 'table1' ,TRUE) ): ?>
            ●
          <?php else: ?>
            ー
          <?php endif; ?>
          </td>
          <td>
          <?php if ( get_post_meta($post->ID, 'table2' ,TRUE) ): ?>
            ●
          <?php else: ?>
            ー
          <?php endif; ?>
          </td>
          <td>
          <?php if ( get_post_meta($post->ID, 'table3' ,TRUE) ): ?>
            ●
          <?php else: ?>
            ー
          <?php endif; ?>
          </td>
          <td>
          <?php if ( get_post_meta($post->ID, 'table4' ,TRUE) ): ?>
            ●
          <?php else: ?>
            ー
          <?php endif; ?>
          </td>
          <td>
          <?php if ( get_post_meta($post->ID, 'table5' ,TRUE) ): ?>
            ●
          <?php else: ?>
            ー
          <?php endif; ?>
          </td>
          <td>
          <?php if ( get_post_meta($post->ID, 'table6' ,TRUE) ): ?>
            ●
          <?php else: ?>
            ー
          <?php endif; ?>
          </td>
        </tr>
      </table>
      <div class="c-about-us js-anim_elm -base">
        <h3 class="c-about-us__title c-text03">About us</h3>
        <p class="c-text01"><?php the_content(); ?></p>
        <?php if(get_post_meta($post->ID,'official_site_url',true)): ?>
        <div class="c-about-us__button c-button02 -green">
          <a class="js-hover" target="_blank" href="<?php echo $cfs->get('official_site_url'); ?>">
            <span class="js-sprit">公式ホームページ</span>
            <i class="ico"><img class="js-svg" src="../../img/common/ico/external-link-alt-regular.svg" alt=""></i>
          </a>
        </div>
        <?php else : ?>
        <?php endif; ?>
      </div>
    </div>
  </div>
  <div class="c-container">
  <?php $args = array(
    'post_type' => 'member',
    'posts_per_page' => 3,
    'paged' => $paged,
    'post__not_in' => array(get_the_ID())
    );
    if (!empty($terms)) {
      $args['tax_query'] = array(
        array(
          'field' => 'slug',
          'taxonomy' => 'area_cat',
          'terms' => array($terms[0]->slug)
        )
      );
    }
    $the_query = new WP_Query($args); if ($the_query->have_posts()) : ?>
    <div class="p-member_single__other">
      <h2 class="c-title05 c-title-center js-anim_elm -base">
        <span class="en">Others</span>
        <span class="jp">近くの保育園</span>
      </h2>
        <ul class="p-member__list">
        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
          <li class="item js-anim_elm -order-fade">
            <a class="js-hover" href="<?php the_permalink(); ?>">
              <div class="item__img">
                <?php if(has_post_thumbnail()): ?>
                <img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x,<?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
                <?php endif; ?>
              </div>
              <p class="item__text c-text02"><i class="ico"><img class="js-svg" src="../../img/common/ico/chevron-circle-right-light.svg" alt=""></i><?php the_title(); ?></p>
            </a>
          </li>
          <?php endwhile; ?>
        </ul>
        <?php endif; wp_reset_postdata(); ?>
      <div class="c-button01 -green -small -center js-anim_elm -base">
        <a class="js-hover" href="/member/">
          一覧へ
          <i class="ico"><img class="js-svg" src="../../img/common/ico/chevron-circle-right-light.svg" alt=""></i>
        </a>
      </div>
    </div>
  </div>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php endwhile; endif; get_footer(); ?>