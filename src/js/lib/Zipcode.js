export default class {
  constructor() {
    const zip = document.getElementById('js-zipcode')
    if (zip) {
      zip.addEventListener('blur', function () {
        AjaxZip3.zip2addr(this, '', 'your_address', 'your_address')
      })
    }
  }
}
