<?php /*
Template Name: Archive
*/
$paged = get_query_var('paged', 1); get_header(); ?>

<?php
$category_name = get_query_var('category_name');
$post_type = get_post_type();

if($post_type == 'news_qa'){
	$terms = get_terms('news_qa_cat', array('hide_empty' => false));
	$current_terms = get_terms($post->ID,'news_qa_cat');
	foreach($current_terms as $current_term){
		$current_name = esc_html($current_term->name);
		$current_slug = esc_html($current_term->slug);
	}
}else{
	$terms = get_terms('category', array('hide_empty' => false,'parent' => 4));
}
?>

<div class="c-lower-head02">
  <div class="c-container">
		<?php if($post_type == 'news_qa'):?>
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">News / Q&amp;A</span>
      <span class="jp c-title01 c-text-center">お知らせ・よろず相談</span>
    </h1>
		<?php else: ?>
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">News</span>
      <span class="jp c-title01 c-text-center">お知らせ</span>
    </h1>
    <?php endif; ?>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-news_archive js-anim_elm -base">
  <div class="c-container">
		<?php  if (!empty($terms)) : ?>
    <ul class="p-news_archive__list">
			<?php if ($post_type == 'news_qa') : ?>
				<li class="p-news_archive__list__item <?php echo $term->name; ?>">
					<a class="js-hover" href="<?php echo esc_url(home_url('news_qa')); ?>">
						全て
					</a>
				</li>
				<?php foreach ($terms as $term) : ?>
				<li class="p-news_archive__list__item <?php echo esc_html($term->name); ?>">
					<a class="js-hover <?php if($term->name == $current_slug){ echo 'is-current';}?>" href="?news_qa_cat=<?php echo esc_html($term->slug); ?>">
						<?php echo $term->name; ?>
					</a>
				</li>
				<?php endforeach;?>
			<?php else: ?>
				<li class="p-news_archive__list__item <?php if(isset($term->name)){ echo $term->name;} ?>">
					<a <?php if ($category_name == 'info') { echo 'class="is-current"';} ?>class="js-hover" href="<?php echo esc_url(home_url('info')); ?>">
						全て
					</a>
				</li>
				<?php foreach ($terms as $term) : ?>
				<li class="p-news_archive__list__item <?php echo $term->name; ?>">
					<a class="js-hover <?php if ($term->slug == $category_name) { echo 'class="is-current"';} ?>" href="<?php echo esc_url(home_url('info/' . $term->slug)); ?>">
						<?php echo $term->name; ?>
					</a>
				</li>
				<?php endforeach;?>
			<?php endif; ?>
    </ul>
		<?php endif; ?>

		<?php if (have_posts()) : ?>
    <div id="<?php echo $term->name; ?>" class="p-news_archive__content">
      <ul class="c-news__list">
				<?php while (have_posts()) : the_post(); ?>
				<?php locate_template('parts/item-news.php', true, false); ?>
				<?php endwhile; ?>
			</ul>
			<?php if (function_exists('wp_pagenavi')) : ?>
			<?php wp_pagenavi(); ?>
			<?php endif; ?>
		</div>
		<?php else : ?>
		<p class="c-text01">まだ記事がありません。</p>
    <?php endif; wp_reset_postdata(); ?>
	</div>
</article>
<?php get_footer(); ?>
