<?php
/*
 Union Theme - Version: 1.4
*/

//テーマセットアップ
function uniontheme_setup() {

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

	// This theme uses a custom image size for featured images, displayed on "standard" posts.
	add_theme_support( 'post-thumbnails' );

}
add_action( 'after_setup_theme', 'uniontheme_setup' );

//プラグインの更新を非表示/
// add_action('admin_menu', 'remove_counts');
// function remove_counts(){
//   global $menu,$submenu;
//   $menu[65][0] = 'プラグイン';
//   $submenu['index.php'][10][0] = '更新';
// }

//wp_head非表示項目
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles', 10);

//カテゴリーの階層を保持
function lig_wp_category_terms_checklist_no_top( $args, $post_id = null ) {
    $args['checked_ontop'] = false;
    return $args;
}
add_action( 'wp_terms_checklist_args', 'lig_wp_category_terms_checklist_no_top' );

//wordpressのjqueryを使わない
function no_wp_jquery() {
  if(!is_admin()){
    wp_deregister_script( 'jquery' );
  }
}
// add_action('wp_enqueue_scripts','no_wp_jquery');

//ヘッダーにcommonを読込む
function common_scripts() {
  if(!is_admin() || is_404()){
    wp_deregister_script( 'jquery' );
    wp_enqueue_script('scripts',esc_url(home_url('/')).'dist/js/bundle.js');
  }
}
add_action('wp_footer','common_scripts');

function common_styles() {
  if(!is_admin() || is_404()){
    wp_enqueue_style('default',esc_url(home_url('/')).'dist/css/style.css');
    wp_enqueue_style('builtin',esc_url(get_stylesheet_uri()));
  }
}
add_action('wp_print_styles','common_styles');

//年月日アーカイブのタイトル日本語表記を整える
function ja_date_wp_title($title, $sep, $seplocation) {
  $year = get_query_var('year');
  $monthnum = get_query_var('monthnum');
  $day = get_query_var('day');

  // from wp-includes/general-template.php:wp_title()
  if ( is_archive() && !empty($year) ) {
    $title = $year . "年";
    if ( !empty($monthnum) )
      $title .= zeroise($monthnum, 2) . "月";
    if ( !empty($day) )
      $title .= zeroise($day, 2) . "日";

    if ($seplocation == 'right') {
      $title = $title . ' ' . $sep . ' ';
    } else {
      $title = $sep . ' ' . $title . ' ';
    }
  }

  return $title;
}
add_filter('wp_title', 'ja_date_wp_title', 10, 3);

// フィルタの登録
add_filter('content_save_pre','tag_save_pre');

function tag_save_pre($content){
  global $allowedposttags;

  // iframeとiframeで使える属性を指定する
  $allowedposttags['iframe'] = array('class' => array () , 'src'=>array() , 'width'=>array(),
  'height'=>array() , 'frameborder' => array() , 'scrolling'=>array(),'marginheight'=>array(),
  'marginwidth'=>array());

  return $content;
}

// 「コメント」と「ツール」を非表示にする
function remove_menus () {
  if (!current_user_can('level_10')) {
    //管理者以外のユーザーの場合メニューをunsetする
    global $menu;
    unset($menu[25]); // コメント
    unset($menu[75]); // ツール
  }
}
add_action('admin_menu', 'remove_menus');

  //改行なし、タグ削除、文字数制限
function strim($str,$size=100,$end="...") {
  return mb_strimwidth(esc_html(strip_tags(strip_shortcodes($str))),0,$size,$end,'utf-8');
}

//コピーライト年号取得
function get_year($start){
  $year = date('Y');
  if($start != $year){
    return $start.' - '.$year;
  }else{
    return $start;
  }
}

//カスタム分類のラベルをwp_titleから削除
add_filter( 'wp_title', 'fix_wp_title', 10, 3 );
function fix_wp_title($title, $sep, $seplocation){
  global $wp_query;
  if ( is_tax() ) {
    $term = $wp_query->get_queried_object();
    $term = $term->name;
    $title =$term;
    $t_sep = '%WP_TITILE_SEP%'; // Temporary separator, for accurate flipping, if necessary

    $prefix = '';
    if ( !empty($title) )
      $prefix = " $sep ";
    if ( 'right' == $seplocation ) {
      $title_array = explode( $t_sep, $title );
      $title_array = array_reverse( $title_array );
      $title = implode( " $sep ", $title_array ) . $prefix;
    } else {
      $title_array = explode( $t_sep, $title );
      $title = $prefix . implode( " $sep ", $title_array );
    }
  }
  return $title;
}

// wp_list_pages からtitle属性を削除
function delete_list_page_title_attribute( $output ) {
  $output = preg_replace( '/ title="[^"]*"/', '', $output );
  return $output;
}
add_filter( 'wp_list_pages', 'delete_list_page_title_attribute' );

// wp_list_categories からtitle属性を削除
function delete_list_categories_title_attribute( $output ) {
  $output = preg_replace( '/ title="[^"]*"/', '', $output );
  return $output;
}
add_filter( 'wp_list_categories', 'delete_list_categories_title_attribute' );

//ホームURLを出力するショートコード
function user_fields_shortcode_home_url() {
  return esc_url( home_url( '/' ) );
}
add_shortcode( 'home_url', 'user_fields_shortcode_home_url' );

/***************************************
 カスタムショートコード設定終わり
***************************************/

//ホームURLを定数化
define('HOME',esc_url( home_url( '/' ))); //サイトURL＝HOME
define('THEMEDIR',esc_url(get_template_directory_uri()).'/'); //テーマディレクトリURL＝THEMEDIR

//管理画面のWP更新メッセージを非表示に
add_action('admin_print_styles', 'admin_css_custom');
function admin_css_custom() {
echo '<style>#update-nag, .update-nag{display: none !important;}</style>';
}

//言語ファイルの自動アップデートを停止
add_filter( 'auto_update_translation', '__return_false' );


//「URL/login」「URL/admin」「URL/dashboard」へのリダイレクト禁止
remove_action( 'template_redirect', 'wp_redirect_admin_locations', 1000 );


// wp-captionのカスタマイズ
add_shortcode('caption', 'custom_caption_shortcode');

function custom_caption_shortcode($attr, $content = null) {
  if ( ! isset( $attr['caption'] ) ) {
    if ( preg_match( '#((?:<a [^>]+>s*)?<img [^>]+>(?:s*</a>)?)(.*)#is', $content, $matches ) ) {
      $content = $matches[1];
      $attr['caption'] = trim( $matches[2] );
    }
  }

  $output = apply_filters('img_caption_shortcode', '', $attr, $content);
  if ( $output != '' )
    return $output;

  extract(shortcode_atts(array(
    'id'    => '',
    'align' => 'alignnone',
    'width' => '',
    'caption' => ''
  ), $attr, 'caption'));

  if ( 1 > (int) $width || empty($caption) )
    return $content;

  if ( $id ) $id = 'id="' . esc_attr($id) . '" ';

  return '<figure ' . $id . 'class="wp-caption ' . esc_attr($align) . '">' . do_shortcode( $content ) . '<figcaption class="wp-caption-text">' . $caption . '</figcaption></figure>';
}


function remove_cssjs_ver2( $src ) {
  if ( strpos( $src, 'ver=' ) )
      $src = remove_query_arg( 'ver', $src );
  return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver2', 9999 );
add_filter( 'script_loader_src', 'remove_cssjs_ver2', 9999 );

function gutenberg_support_setup() {
  //Gutenberg用スタイルの読み込み
  add_theme_support( 'wp-block-styles' );
  //add_theme_support( 'align-wide' );
  add_theme_support('editor-styles');
  //独自スタイルの適用
  add_editor_style();
}
add_action( 'after_setup_theme', 'gutenberg_support_setup' );

#function myguten_enqueue() {
#	echo '<script>
#	console.log("font-size changed!");
#	document.documentElement.style.fontSize = "62.5%";
#	</script>';
#}
#add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );
function myguten_enqueue() {
  global $post_type;
  echo '<script>
	console.log("font-size changed!");
	document.documentElement.style.fontSize = "62.5%";
	</script>';
  if ($post_type == 'support-company') {
    echo '<script>window.addEventListener(\'load\', function () {if (document.querySelector(\'.block-editor-block-list__layout\') == null) return;document.querySelector(\'.block-editor-block-list__layout\').classList.add(\'support-company\')})</script>';
  }
}
add_action( 'enqueue_block_editor_assets', 'myguten_enqueue' );
// 管理バーを下に
function oz_admin_bar_to_the_bottom() {
  echo '<style type="text/css">
  html {
    margin-top: 0 !important;
  }
  #wpadminbar {
    top: auto !important;
    position: fixed !important;
    bottom: 0 !important;
  }
  #wpadminbar .ab-sub-wrapper {
    bottom: 100% !important;
  }
  </style>';
}
// on frontend area
add_action( 'wp_footer', 'oz_admin_bar_to_the_bottom' );


//固定ページでテンプレートをショートコード呼び出し
function Include_my_php($params = array()) {
  extract(shortcode_atts(array(
      'file' => 'default'
  ), $params));
  ob_start();
  include(get_theme_root() . '/' . get_template() . "/$file.php");
  return ob_get_clean();
}
add_shortcode('myphp', 'Include_my_php');

//親スラッグ取得
function is_parent_slug() {
  global $post;
  if (isset($post->post_parent)) {
    $post_data = get_post($post->post_parent);
    return $post_data->post_name;
  }
}

//セミナー　おすすめ
function my_connection_types() {
  // Posts 2 Posts プラグインが有効化されてるかチェック
  if ( !function_exists( 'p2p_register_connection_type' ) )
    return;

  // 登録する
  p2p_register_connection_type(
    array(
      'name' => 'posts_to_seminar',
      'from' => 'seminar',
      'to' => 'seminar'
    )
  );
  p2p_register_connection_type(
    array(
      'name' => 'posts_to_member_seminar',
      'from' => 'member_seminar',
      'to' => 'member_seminar'
    )
  );
}
add_action( 'wp_loaded', 'my_connection_types' );


/**
 * ログイン処理をまとめた関数
 */
function my_user_login() {
  $user_name = isset( $_POST['user_name'] ) ? sanitize_text_field( $_POST['user_name'] ) : '';
  $user_pass = isset( $_POST['user_pass'] ) ? sanitize_text_field( $_POST['user_pass'] ) : '';
  // ログイン認証
  $creds = array(
    'user_login' => $user_name,
    'user_password' => $user_pass,
  );
  $user = wp_signon( $creds );
  //ログイン失敗時の処理
  if ( is_wp_error( $user ) ) {
    echo $user->get_error_message();
    exit;
  }
  //ログイン成功時の処理
  wp_redirect( '/mypage' );
  exit;
  return;
}

/**
* after_setup_theme に処理をフック
*/
add_action('after_setup_theme', function() {
  if ( isset( $_POST['my_submit'] ) && $_POST['my_submit'] === 'login') {
    // nonceチェック
    if ( !isset( $_POST['my_nonce_name'] ) ) return;
    if ( !wp_verify_nonce( $_POST['my_nonce_name'], 'my_nonce_action' ) ) return;
    // ログインフォームからの送信があれば
    my_user_login();
  }
});

function loggedin_check() {
  // ログインしていないことを確認
  $dir_name = $_SERVER['REQUEST_URI'];

  if( preg_match( '{/wp/login}', $dir_name) ) {
    return;
  }else{
    if( preg_match( '/mypage/', $dir_name) ) {
      if ( !is_user_logged_in() ) {
        // REQUEST_URIに『lostpassword』『logout』という文字列が含まれていないか判定
        // $dir_name = $_SERVER['REQUEST_URI'];

        wp_redirect( '/login', 301 );
        exit;
      }
    }
    if( preg_match( '/login/', $dir_name) ) {
      if ( is_user_logged_in() ) {
        if( preg_match( '/action=logout/', $dir_name)  ) {
          return;
        }
        wp_redirect( '/mypage', 301 );
        exit;
      }
    }
  }
}
add_filter( 'init', 'loggedin_check' );

