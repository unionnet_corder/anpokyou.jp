<?php
get_header(); ?>

<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head01__title c-title04">
      <span class="en c-text-center">Member</span>
      <span class="jp c-title01 c-text-center">会員一覧</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-member js-anim_elm -base">
  <div class="c-container">
    <div class="c-search">
      <dl class="c-search__item">
        <dt class="c-search__item__title">都道府県</dt>
        <dd class="c-search__item__content">
        <select class="c-select js-location_select" name="pref">
            <option value="/member" selected>全て</option>
            <?php $area_terms = get_terms('area_cat','hide_empty=0');
            foreach ($area_terms as $term):
              $name = esc_html($term->name);
              $slug = esc_html($term->slug);
            ?>
            <option value="/member/?area_cat=<?php echo $slug; ?>"><?php echo $name; ?></option>
            <?php endforeach;?>
          </select>
        </dd>
      </dl>
    </div>
		<?php if (have_posts()) : ?>
    <ul class="p-member__list">
      <?php while (have_posts()) : the_post(); ?>
      <li class="item js-anim_elm -order-fade">
        <a class="js-hover" href="<?php the_permalink(); ?>">
          <div class="item__img">
            <?php if(has_post_thumbnail()): ?>
						<img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x,<?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
						<?php endif; ?>
          </div>
          <p class="item__text c-text02"><i class="ico"><img class="js-svg" src="../img/common/ico/chevron-circle-right-light.svg" alt=""></i><?php the_title(); ?></p>
          <ul class="item__tag">
            <li>#<?php if ($terms = get_the_terms($post->ID, 'area_cat')) {
                    foreach ($terms as $term) {
                      echo $term->name;
                    }
                } ?>
            </li>
          </ul>
        </a>
      </li>
      <?php endwhile; ?>
    </ul>
    <?php if (function_exists('wp_pagenavi')) : ?>
      <?php wp_pagenavi(); ?>
    <?php endif; ?>
		<?php else : ?>
		<p class="c-text01 u-mt20">まだ記事がありません。</p>
		<?php endif; wp_reset_postdata(); ?>
  </div>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php get_footer(); ?>
