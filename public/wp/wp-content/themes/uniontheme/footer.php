<?php
/*
 Union Theme - Version: 1.4
*/
?>
<div class="c-navi__bottom">
  <ul class="c-navi__bottom__list">
    <li class="item">
      <a class="js-hover" href="<?php echo HOME; ?>login">
        <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/sign-in-alt-light.svg" alt=""></span>
        <p><span class="js-sprit">会員ログイン</span></p>
      </a>
    </li>
    <li class="item">
      <a class="js-hover" href="./contact">
        <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/envelope-light.svg" alt=""></span>
        <p class="js-sprit">お問い合わせ</p>
      </a>
    </li>
  </ul>
</div>
<!-- /.l-main --></main>

<footer class="l-footer">

  <div class="c-container">
    <div class="l-footer_bottom">
      <div class="l-footer_bottom__logo">
        <a href="<?php echo HOME; ?>">
          <img src="<?php echo HOME; ?>img/common/logo.svg" alt="一般社団法人　安全保育推進連絡協議会">
        </a>
      </div>
      <div class="l-footer_bottom__sitemap">
        <ul class="l-footer_bottom__sitemap__list">
          <li class="item">
            <a href="<?php echo HOME; ?>about-us/">組合情報</a>
          </li>
          <li class="item">
            <a href="<?php echo HOME; ?>member/">組合企業一覧</a>
          </li>
          <li class="item">
            <a href="<?php echo HOME; ?>support-company/">賛助企業</a>
          </li>
        </ul>
        <ul class="l-footer_bottom__sitemap__list">
          <li class="item">
            <a href="<?php echo HOME; ?>info/">新着情報</a>
          </li>
          <li class="item">
            <a href="<?php echo HOME; ?>seminar/">セミナー・研修</a>
          </li>
        </ul>
        <ul class="l-footer_bottom__sitemap__list">
          <li class="item">
            <a href="<?php echo HOME; ?>join-us/">入会希望の方</a>
          </li>
          <li class="item">
            <a href="<?php echo HOME; ?>privacy/">プライバシーポリシー</a>
          </li>
          <?php if ( is_user_logged_in() ) : ?>
          <li class="c-button01 -small -ico-left">
            <a class="js-hover" href="<?php echo HOME; ?>mypage/">
              <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/user-light.svg" alt=""></i>
              マイページ</a>
          </li>
          <?php else: ?>
            <li class="c-button01 -small -ico-left">
              <a class="js-hover" href="<?php echo HOME; ?>login/">
                <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/sign-in-alt-light.svg" alt=""></i>
                会員ログイン画面</a>
            </li>
          <?php endif; ?>
        </ul>
      </div>
    </div>
    <p class="l-footer_bottom__copy">&copy; 一般社団法人 安全保育推進連絡協議会</p>
  </div>
<!-- / .l-footer --></footer>
<div class="c-side_navi">
  <div class="c-drawer__button js-drawer-button js-hover">
    <div class="c-drawer__humburger">
      <span></span>
    </div>
    <span class="c-drawer__humburger_menu js-sprit"></span>
  </div>
  <div class="c-side_navi__copy">
    <p class="c-font-zen">&copy; 一般社団法人 安全保育推進連絡協議会</p>
  </div>
  <div class="c-side_navi__bottom">
    <ul class="c-side_navi__bottom__list">
      <li class="item">
        <?php if ( is_user_logged_in() ) :?>
          <a href="<?php echo wp_logout_url( home_url() ); ?>">
            <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/sign-in-alt-light.svg" alt=""></span>
            <p>
              <span class="js-sprit">会員</span><br>
              <span class="js-sprit">ログアウト</span>
            </p>
          </a>
        <?php else: ?>
          <a href="<?php echo HOME; ?>login/">
          <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/sign-in-alt-light.svg" alt=""></span>
          <p>
            <span class="js-sprit">会員</span><br>
            <span class="js-sprit">ログイン</span>
          </p>
        </a>
        <?php endif; ?>
      </li>
      <li class="item">
        <a href="<?php echo HOME; ?>contact/">
          <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/envelope-light.svg" alt=""></span>
          <p class="js-sprit">お問い合わせ</p>
        </a>
      </li>
    </ul>
  </div>
</div>
<nav id="js-drawer" class="c-drawer">
  <div class="c-drawer__container">
    <div class="c-drawer__inner">
      <div class="c-drawer__img">
        <img src="<?php echo HOME; ?>img/common/parts/drawer.jpg" srcset="<?php echo HOME; ?>img/common/parts/drawer.jpg 1x,<?php echo HOME; ?>img/common/parts/drawer@2x.jpg 2x" alt="">
      </div>
      <div class="c-drawer__menu">
        <div class="c-drawer__menu__inner">
          <div class="c-drawer__menu__head">
            <ul class="c-drawer__menu__list">
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>about-us/">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  総合情報
                </a>
              </li>
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>member/">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  組合企業一覧
                </a>
              </li>
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>support-company/">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  賛助企業
                </a>
              </li>
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>info/">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  新着情報
                </a>
              </li>
            </ul>
            <ul class="c-drawer__menu__list">
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>seminar/">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  セミナー・研修
                </a>
              </li>
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>join-us/">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  入会希望の方
                </a>
              </li>
              <li class="c-drawer__menu__item">
                <a class="js-hover" href="<?php echo HOME; ?>privacy">
                  <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
                  プライバシーポリシー
                </a>
              </li>
            </ul>
          </div>
          <div class="c-drawer__menu__bottom">
            <div class="c-drawer__menu__bottom__item">
              <?php if ( is_user_logged_in() ) : ?>
              <a class="js-hover" href="<?php echo HOME; ?>mypage/">
                <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/user-light.svg" alt=""></span><br>
                マイページ
              </a>
              <?php else: ?>
                <a class="js-hover" href="<?php echo HOME; ?>login/">
                  <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/sign-in-alt-light.svg" alt=""></span><br>
                  会員ログイン
                </a>
              <?php endif; ?>
            </div>
            <div class="c-drawer__menu__bottom__item">
              <a class="js-hover" href="<?php echo HOME; ?>contact/">
                <span class="item_svg"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/envelope-light.svg" alt=""></span><br>
                お問い合わせ
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</nav>

<!-- / #page --></div>

<?php wp_footer(); ?>
<!-- <script src="<?php echo HOME; ?>dist/js/bundle.js" charset="utf-8"></script> -->
</body>
</html>