const chokidar = require('chokidar')
const keepfolder = require('imagemin-keep-folder')
const mozjpeg = require('imagemin-mozjpeg')
const pngquant = require('imagemin-pngquant')
const gifsicle = require('imagemin-gifsicle')

const watcher = chokidar.watch('src/img/**/*.{jpg,png,gif,svg}')

const compress = function (path = 'src/img/**/*.{jpg,png,gif,svg}') {
  keepfolder([path], {
    plugins: [
      mozjpeg({
        quality: 80,
      }),
      pngquant({
        quality: [0.7, 0.8],
      }),
      gifsicle(),
    ],
    replaceOutputDir: (output) => {
      return output.replace(/img\//, '../public/img/')
    },
  })
}

compress()

watcher.on('ready', () => {
  //スタンバイ状態
  console.log('ready watching image...')

  //ファイルを追加したとき
  watcher.on('add', (path) => {
    console.log('add:' + path)
    compress(path)
  })

  //ファイルの編集
  watcher.on('change', (path) => {
    console.log('change:' + path)
    compress(path)
  })
})
