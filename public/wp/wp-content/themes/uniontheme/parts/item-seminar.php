<li class="c-seminar_list__item">
  <a href="<?php the_permalink(); ?>" class="c-seminar_list__link js-hover">
    <div class="c-seminar_list__link__img">
    <?php if(has_post_thumbnail()): ?>
  <img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x,<?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
  <?php endif; ?>
    </div>
    <div class="c-seminar_list__link__text">
      <dl class="c-seminar_list__link__desc">
        <dt>開催日</dt>
        <dd><time datetime="<?php echo date('Y年m月d日',strtotime(CFS()->get('date'))); ?>"><?php echo date('Y年m月d日',strtotime(CFS()->get('date'))); ?></time></dd>
      </dl>
      <dl class="c-seminar_list__link__desc">
        <dt>開催場所</dt>
        <dd><?php echo CFS()->get('place'); ?></dd>
      </dl>
      <p class="c-seminar_list__link__title c-text02"><?php the_title(); ?></p>
    </div>
  </a>
</li>