const sortCssMediaQueries = require('sort-css-media-queries');
module.exports = {
  plugins: [
    require('autoprefixer')({
      cascade: false,
    }),
    require('css-mqpacker')({
      sort: sortCssMediaQueries.desktopFirst,
    }),
    require('cssnano')({
      preset: 'default',
    }),
  ],
};
