<?php
 get_header(); if (have_posts()) : while (have_posts()) : the_post(); $cfs = CFS(); ?>
<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">Supporting company</span>
      <span class="jp c-title01 c-text-center">賛助企業詳細</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-support-company_single js-anim_elm -base">
  <div class="c-container">
    <div class="p-support-company_single__content">
      <div class="p-support-company_single__img js-anim_elm -base">
        <?php if(has_post_thumbnail()): ?>
        <img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x, <?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
        <?php endif; ?>
      </div>
      <div class="p-support-company_single__info">
        <p class="p-support-company_single__info__profession js-anim_elm -base"><?php echo $cfs->get('business'); ?></p>
        <h2 class="p-support-company_single__info__company js-anim_elm -base"><?php the_title(); ?></h2>
        <dl class="p-support-company_single__info__detail js-anim_elm -base">
          <dt class="title c-text02">所在地</dt>
          <dd class="text c-text02"><?php echo $cfs->get('address'); ?></dd>
        </dl>
        <dl class="p-support-company_single__info__detail js-anim_elm -base">
          <dt class="title c-text02">TEL</dt>
          <dd class="text c-text02"><?php echo $cfs->get('tel'); ?></dd>
        </dl>
        <?php if(get_post_meta($post->ID,'google_map_url',true)): ?>
        <div class="c-button03 js-anim_elm -base">
          <a target="_blank" class="js-hover" href="<?php echo $cfs->get('google_map_url'); ?>">
            <i class="ico"><img class="js-svg" src="../../img/common/ico/map-pin.svg" alt=""></i>Google Maps
          </a>
        </div>
        <?php else : ?>
        <?php endif; ?>
        <ul class="p-support-company_single__info__sns js-anim_elm -base">
          <?php if(get_post_meta($post->ID,'instagram',true)): ?>
          <li class="item">
            <a target="_blank" class="js-hover" href="<?php echo $cfs->get('instagram'); ?>">
              <img class="js-svg" src="../../img/common/ico/instagram-brands.svg" alt="">
            </a>
          </li>
          <?php else : ?>
          <?php endif; ?>
          <?php if(get_post_meta($post->ID,'facebook',true)): ?>
          <li class="item">
            <a target="_blank" class="js-hover" href="<?php echo $cfs->get('facebook'); ?>">
              <img class="js-svg" src="../../img/common/ico/facebook.svg" alt="">
            </a>
          </li>
          <?php else : ?>
          <?php endif; ?>
        </ul>
        <?php if(get_post_meta($post->ID,'company_url',true)): ?>
        <div class="p-support-company_single__info__link">
          <div class="c-button01 js-anim_elm -base -medium">
            <a class="js-hover" target="_blank" href="<?php echo $cfs->get('company_url'); ?>">
              この企業に問い合わせる
              <i class="ico"><img class="js-svg" src="../../img/common/ico/external-link-alt-regular.svg" alt=""></i>
            </a>
          </div>
          <p class="c-text01 js-anim_elm -base">(企業HPへ)</p>
        </div>
        <p class="p-support-company_single__info__notice c-text01 js-anim_elm -base">安全保育推進連絡協議会の会員とお伝えください。<br>会員得点が受けられない場合がございます。</p>
        <?php else : ?>
        <?php endif; ?>
      </div>
    </div>
    <div class="c-about-us js-anim_elm -base">
      <h3 class="c-about-us__title c-text03">About us</h3>
      <div class="c-text01">
        <?php the_content(); ?>
      </div>
    </div>
    <div class="p-support-company_single__prev c-button01 -green -small js-anim_elm -base">
      <a class="js-hover" href="<?php echo HOME; ?>support-company/">
        一覧へ
        <i class="ico"><img class="js-svg" src="../../img/common/ico/chevron-circle-right-light.svg" alt=""></i>
      </a>
    </div>
  </div>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php endwhile; endif; get_footer(); ?>