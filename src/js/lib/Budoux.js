import { loadDefaultJapaneseParser } from 'budoux'
import { IsIE } from './Ua'

// テキストのみが入っているタグに使用してください。
// NG: <span class="js-budoux">あいうえおかき<span class="mark">くけこさ</span>しすせそ</span>
// NG: <span class="js-budoux">あいうえおかきくけ<br>こさしすせそ</span>
//     この場合は、改行タグなどはないものと認識されます。
// OK: <span class="js-budoux">あいうえおかきくけこさしすせそ</span>

export default class {
  constructor() {
    if (IsIE()) return
    // <wbr>タグはIEで使えないので、IEの場合は停止
    document.addEventListener('DOMContentLoaded', function () {
      if (document.getElementsByClassName('js-budoux').length == 0) return
      const parser = loadDefaultJapaneseParser()
      const budoux = document.getElementsByClassName('js-budoux')
      const length = budoux.length
      let array
      for (var i = 0; i < length; i++) {
        array = parser.parse(budoux[i].textContent)
        if (array.length > 1) {
          // 文字列によっては、一つも分割してくれないこともあるため、二つ以上に分割されている場合のみ
          // keep-allと<wbr>タグ挿入
          // 分割されていない状態でkeep-allをすると、white-space:nowrap;みたいになります。
          budoux[i].style.wordBreak = 'keep-all'
          budoux[i].innerHTML = array.join('<wbr>')
        }
      }
    })
  }
}
