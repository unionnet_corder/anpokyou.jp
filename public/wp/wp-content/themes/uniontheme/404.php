<?php 
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>

<section class="p-notfound_body">
  <div class="c-container">
    <h1>404 Not Found</h1>
    <h2>404 Not Found - ページが見つかりません</h2>
    <div class="body">
      <p>指定されたページまたはファイルは存在しません</p>
      <ul>
      <li>URL、ファイル名にタイプミスがないかご確認ください。</li>
      <li>指定されたページは削除されたか、移動した可能性があります。</li>
      </ul>
      <div class="p-contact__submit">
        <div class="c-button01 -green -small -center">
          <a class="js-hover" href="<?php echo HOME; ?>">
            TOPへ
            <i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i>
          </a>
        </div>
     </div>
    <!-- / .body --></div>
  </div>
<!-- /.p-notfound_body --></section>

<?php get_footer(); ?>