<?php
/*
 Union Theme - Version: 1.4
*/
get_header(); ?>


<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">Union member</span>
      <span class="jp c-title01 c-text-center">会員専用ページ</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<section class="p-mypage_news">
  <div class="c-container">
    <div class="p-mypage_news__container">
      <div class="p-mypage_news__title">
        <div class="c-title01 js-anim_elm -base">
          <p class="c-title01__sub js-sprit js-anim_elm -blur-text">News / Q&A</p>
          <h2 class="c-title01__lead js-anim_elm -blur -blur_bottom">お知らせ・よろず相談</h2>
        </div>
        <div class="c-button02 js-anim_elm -base">
          <a class="js-hover" href="<?php echo HOME; ?>mypage/news_qa"><span class="js-sprit">一覧へ</span><i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i></a>
        </div>
      </div>
      <div class="p-mypage_news__content js-anim_elm -base">
        <ul class="c-news_list">
          <?php
            $args = array(
              'post_type' => 'news_qa',
              'posts_per_page' => 3,
            );
            $the_query = new WP_Query($args);
          ?>
          <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
              <?php locate_template( 'parts/item-news.php', true, false ); ?>
            <?php endwhile; ?>
          <?php endif; wp_reset_postdata(); ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<section class="p-mypage_seminar">
  <div class="c-container">
    <div class="p-mypage_seminar__container">
      <div class="p-mypage_seminar__title">
        <div class="c-title01 js-anim_elm -base">
          <p class="c-title01__sub js-sprit js-anim_elm -blur-text ">Seminar / Training</p>
          <h2 class="c-title01__lead js-anim_elm -blur -blur_bottom ">セミナー・研修</h2>
        </div>
        <div class="c-button02 js-anim_elm -base">
          <a class="js-hover" href="<?php echo HOME; ?>mypage/member_seminar"><span class="js-sprit">一覧へ</span><i class="ico"><img class="js-svg" src="<?php echo HOME; ?>img/common/ico/chevron-circle-right-light.svg" alt=""></i></a>
        </div>
      </div>
      <div class="p-mypage_seminar__content js-anim_elm -base">
        <ul class="p-index_seminar__list">
          <?php
            $args = array(
              'post_type' => 'member_seminar',
              'posts_per_page' => 3,
            );
            $the_query = new WP_Query($args);
          ?>
          <?php if ($the_query->have_posts()) : ?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <?php locate_template( 'parts/item-seminar.php', true, false ); ?>
            <?php endwhile; ?>
          <?php endif; wp_reset_postdata(); ?>
        </ul>
      </div>
    </div>
  </div>
</section>
<?php get_footer(); ?>

