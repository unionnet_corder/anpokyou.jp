import Swiper from 'swiper'

export default function () {
  document.addEventListener('DOMContentLoaded', function () {
    if (document.getElementById('member-slider') == null) return
    const pagination = document.querySelector('.swiper-pagination')
    const swiper = new Swiper('#member-slider', {
      navigation: {
        nextEl: '#member-slider .swiper-button-next',
        prevEl: '#member-slider .swiper-button-prev',
      },
      spaceBetween: 20,
      speed: 1000,
      centeredSlides: false,
      slidesPerView: 1.1,
      breakpoints: {
        767: {
          spaceBetween: 20,
          slidesPerView: 2.5,
          centeredSlides: false,
        },
        1023: {
          spaceBetween: 20,
          slidesPerView: 3,
          centeredSlides: false,
        },
      },
      on: {
        slideChangeTransitionStart: function () {
          pagination.classList.add('is-animation')
        },
        slideChangeTransitionEnd: function () {
          pagination.classList.remove('is-animation')
        },
      },
    })
  })
}
