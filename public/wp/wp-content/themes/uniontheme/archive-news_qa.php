<?php
get_header();
if(isset($_GET['news_qa_cat'])){
  $cat = $_GET['news_qa_cat'];
}
?>

<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">News / Q&amp;A</span>
      <span class="jp c-title01 c-text-center">お知らせ・よろず相談</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-news_archive js-anim_elm -base">
  <div class="c-container">
		<?php
    $terms = get_terms('news_qa_cat', array('hide_empty' => false));
    ?>
    <ul class="p-news_archive__list">
      <li class="p-news_archive__list__item">
        <a class="js-hover <?php if($cat == '' || !$cat) { echo 'is-current';}?>" href="/mypage/news_qa">
					全て
				</a>
      </li>
			<?php foreach ($terms as $term) : ?>
      <li class="p-news_archive__list__item ">
        <a class="js-hover <?php if($cat == $term->slug) { echo 'is-current';}?>" href="/mypage/news_qa/?news_qa_cat=<?php echo esc_html($term->slug); ?>">
        <?php echo esc_html($term->name); ?>
				</a>
      </li>
			<?php endforeach;?>
    </ul>

		<?php if (have_posts()) : ?>
    <div id="<?php echo $term->name; ?>" class="p-news_archive__content">
      <ul class="c-news__list">
				<?php while (have_posts()) : the_post(); ?>
				<?php locate_template('parts/item-news.php', true, false); ?>
				<?php endwhile; ?>
			</ul>
			<?php if (function_exists('wp_pagenavi')) : ?>
			<?php wp_pagenavi(); ?>
			<?php endif; ?>
		</div>
		<?php else : ?>
		<p class="c-text01 u-mt20">まだ記事がありません。</p>
    <?php endif; wp_reset_postdata(); ?>
	</div>
</article>
<?php get_footer(); ?>
