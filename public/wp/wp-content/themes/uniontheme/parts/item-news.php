<li class="c-news_list__item">
  <a href="<?php the_permalink(); ?>" class="c-news_list__link js-hover -flex">
    <div class="c-news_list__link__info">
      <time datetime="<?php the_time('Y-m-d'); ?>"><?php the_time('Y.m.d'); ?></time>
      <?php $terms = get_the_terms(get_the_ID(), 'category'); if (!empty($terms)) : ?>
      <span class="cat"><?php echo $terms[0]->name; ?></span>
      <?php endif; ?>
    </div>
    <div class="c-news_list__link__title c-text02"><?php the_title(); ?></div>
  </a>
</li>