//UA判定
export function UaCheck() {
  let ua = window.navigator.userAgent.toLowerCase()
  return {
    ltIE6: typeof window.addEventListener == 'undefined' && typeof document.documentElement.style.maxHeight == 'undefined',
    ltIE7: typeof window.addEventListener == 'undefined' && typeof document.querySelectorAll == 'undefined',
    ltIE8: typeof window.addEventListener == 'undefined' && typeof document.getElementsByClassName == 'undefined',
    Tablet:
      (ua.indexOf('windows') != -1 && ua.indexOf('touch') != -1 && ua.indexOf('tablet pc') == -1) ||
      ua.indexOf('ipad') != -1 ||
      (ua.indexOf('android') != -1 && ua.indexOf('mobile') == -1) ||
      (ua.indexOf('firefox') != -1 && ua.indexOf('tablet') != -1) ||
      ua.indexOf('kindle') != -1 ||
      ua.indexOf('silk') != -1 ||
      ua.indexOf('playbook') != -1,
    Mobile:
      (ua.indexOf('windows') != -1 && ua.indexOf('phone') != -1) ||
      ua.indexOf('iphone') != -1 ||
      ua.indexOf('ipod') != -1 ||
      (ua.indexOf('android') != -1 && ua.indexOf('mobile') != -1) ||
      (ua.indexOf('firefox') != -1 && ua.indexOf('mobile') != -1) ||
      ua.indexOf('blackberry') != -1,
  }
}

export function IsIE() {
  return navigator.userAgent.indexOf('MSIE ') > -1 || navigator.userAgent.indexOf('Trident/') > -1
}
