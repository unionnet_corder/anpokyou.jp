<?php
if(isset($_GET['pref'])){
  $get_area = $_GET['pref'];
}
if(isset($_GET['industry'])){
  $get_industry = $_GET['industry'];
}
$args = array(
  'post_type' => 'support-company',
  'posts_per_page' => 9,
);

$taxquerysp = [];

if(isset($get_area)){
  $taxquerysp[] = array(
    'taxonomy' => 'area_cat',
    'field' => 'slug',
    'terms' => $get_area,
  );
}
if(isset($get_industry)){
  $taxquerysp[] = array(
    'taxonomy' => 'industry',
    'field' => 'slug',
    'terms' => $get_industry,
  );
}
if(!empty($taxquerysp)) { $args['tax_query'] = $taxquerysp; }
$the_query = new WP_Query($args);
get_header(); ?>

<div class="c-lower-head02">
  <div class="c-container">
    <h1 class="c-lower-head02__titl c-title04">
      <span class="en c-text-center">Supporting company</span>
      <span class="jp c-title01 c-text-center">賛助企業一覧</span>
    </h1>
  </div>
</div>
<?php get_template_part('parts/crumbs'); ?>
<article class="p-support-company js-anim_elm -base">
  <div class="c-container">
    <form action="<?php echo HOME; ?>support-company/">
  <div class="c-search">
    <dl class="c-search__item">
      <dt class="c-search__item__title">都道府県</dt>
      <dd class="c-search__item__content">
        <select class="c-select" name="pref">
        <option value="">全て</option>
        <?php $area_terms = get_terms('area_cat','hide_empty=0');
          foreach ($area_terms as $term): 
            $name = esc_html($term->name);
            $slug = esc_html($term->slug);
          ?>
          <option value="<?php echo $slug; ?>" <?php if($get_area == $slug) { echo 'selected';}?>><?php echo $name; ?></option>
          <?php endforeach;?>
        </select>
      </dd>
    </dl>
    <dl class="c-search__item">
      <dt class="c-search__item__title">業種</dt>
      <dd class="c-search__item__content">
        <select class="c-select" name="industry">
        <option value="">全て</option>
        <?php $area_terms = get_terms('industry','hide_empty=0');
          foreach ($area_terms as $term): 
            $name = esc_html($term->name);
            $slug = esc_html($term->slug);
          ?>
          <option value="<?php echo $slug; ?>" <?php if($get_industry == $slug) { echo 'selected';}?>><?php echo $name; ?></option>
          <?php endforeach;?>
        </select>
      </dd>
    </dl>
    <button type="submit" class="c-search__button js-hover">
      検索する<i class="ico"><img class="js-svg" src="../img/common/ico/search.svg" alt=""></i>
    </button>
  </div>
</form>
		<?php if ($the_query->have_posts()) : ?>
    <ul class="p-support-company__list">
      <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
      <?php 
        $area_terms = get_the_terms($post->ID,'area_cat');
        foreach((array)$area_terms as $area_term){
          $area_name =  esc_html($area_term->name);
        }
        $industry_terms = get_the_terms($post->ID,'industry');
        if(!empty($industry_terms)){
          foreach((array)$industry_terms as $industry_term){
            $industry_name =  esc_html($industry_term->name);
          }
        }
      ?>
      <li class="item js-anim_elm -order-fade">
        <a class="js-hover" href="<?php the_permalink(); ?>">
          <div class="item__img">
            <?php if(has_post_thumbnail()): ?>
						<img src="<?php the_post_thumbnail_url('thumb_column'); ?>" srcset="<?php the_post_thumbnail_url('thumb_column'); ?> 1x,<?php the_post_thumbnail_url('thumb_column@2x'); ?> 2x" alt="<?php echo $title; ?>">
						<?php endif; ?>
          </div>
          <p class="item__text c-text02"><i class="ico"><img class="js-svg" src="../img/common/ico/chevron-circle-right-light.svg" alt=""></i><?php the_title(); ?></p>
          <ul class="item__tag">
            <li>#<?php echo $area_name; ?></li>
            <li>#<?php if(isset($industry_name)){ echo $industry_name;} ?></li>
          </ul>
        </a>
      </li>
      <?php endwhile; ?>
    </ul>
    <?php if (function_exists('wp_pagenavi')) : ?>
      <?php wp_pagenavi(array('query' => $the_query)); ?>
    <?php endif; ?>
		<?php else : ?>
		<p class="c-text01 u-mt20">まだ記事がありません。</p>
		<?php endif; wp_reset_postdata(); ?>
  </div>
</article>

<?php get_template_part('parts/footer-head'); ?>
<?php get_footer(); ?>
